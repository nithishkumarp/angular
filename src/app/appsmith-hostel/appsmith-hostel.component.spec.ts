import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AppsmithHostelComponent } from './appsmith-hostel.component';

describe('AppsmithHostelComponent', () => {
  let component: AppsmithHostelComponent;
  let fixture: ComponentFixture<AppsmithHostelComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AppsmithHostelComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(AppsmithHostelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
