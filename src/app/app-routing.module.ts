import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ExampleFormComponent } from './example-form/example-form.component';
import { AppsmithHostelComponent } from './appsmith-hostel/appsmith-hostel.component';
import { CrudComponent } from './crud/crud.component';

const routes: Routes = [
  {path:'formComponent',component:ExampleFormComponent},
  {path:'appsmithHostel',component:AppsmithHostelComponent},
  {path:'crud',component:CrudComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
