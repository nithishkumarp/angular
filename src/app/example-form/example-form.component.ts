import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ApiService } from '../shared/api.service';
import { ResumeData } from './example-form.module';


@Component({
  selector: 'app-example-form',
  templateUrl: './example-form.component.html',
  styleUrls: ['./example-form.component.css']
})
export class ExampleFormComponent implements OnInit {
  profileForm!: FormGroup;
  profileDataObj:ResumeData=new ResumeData;
  allProfile: any;
 constructor(private formBuilder:FormBuilder,private api:ApiService){

 }
  ngOnInit(): void {
    this.profileForm=this.formBuilder.group({
      name:['',Validators.required],
      emailID:['',[Validators.required,Validators.email]],
      mobile:['',[Validators.required,Validators.minLength(10)]],
      qualification:['',Validators.required],
      address:['',Validators.required],
      work_exp:this.formBuilder.array([
        this.formBuilder.group({
          company:['',Validators.required],
          role:['',Validators.required],
          experience:['',Validators.required]
        })
      ])
    })
    this.getdata()
  }
  addwork_exp() {
    this.work_exp.push(this.formBuilder.group({ company:['',Validators.required],
    role:['',Validators.required],
    experience:['',Validators.required]}));
  }
  get work_exp() {
    return this.profileForm.get('work_exp') as FormArray;
  }
  onSubmit(){
    console.log(this.profileForm.value);
    this.addProfile();
  }
  get f()
  {
    return this.profileForm.controls;
  }
  addProfile(){
    this.profileDataObj.name=this.profileForm.value.name;
    this.profileDataObj.emailID=this.profileForm.value.emailID;
    this.profileDataObj.mobile=this.profileForm.value.mobile;
    this.profileDataObj.qualification=this.profileForm.value.qualification;
    this.profileDataObj.address=this.profileForm.value.address;
    this.profileDataObj.work_exp=this.profileForm.value.work_exp;
    this.api.poststudent(this.profileDataObj).subscribe(res=>{
      console.log(res)
      this.profileForm.reset()
      this.getdata()  
      alert("Record added Successfully");
    },
    err=>{
      alert("Something went wrong!!!");
    })
  }
  getdata(){
    this.api.getstudent()
    .subscribe(res=>{
     this.allProfile=res;
    })
  }


  

}
